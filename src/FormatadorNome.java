
public interface FormatadorNome {

	public String formatarNome(String nome, String sobrenome);
	
	public String informal(String nome);
	
	public String respeitoso(String sobrenome, boolean masculino);
	
	public String comTitulo(String titulo, String nome, String sobrenome);
}
