import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AutoridadeTeste {

	Autoridade a1;
	
	@Before
	public void inicializador() {
		a1 = new Autoridade();
		a1.nome = "Felipe";
		a1.sobrenome = "Oliveira";
		a1.titulo = "Magn�fico";
	}
	
	@Test
	public void nomeInformal() {
		assertEquals("Felipe", a1.getTratamento().informal(a1.nome));
	}
	
	@Test
	public void nomeRespeitoso() {
		assertEquals("Sr. Oliveira", a1.getTratamento().respeitoso(a1.sobrenome, true));
	}
	
	@Test
	public void nomeComTitulo() {
		assertEquals("Magn�fico Felipe Oliveira", a1.getTratamento().comTitulo(a1.titulo, a1.nome, a1.sobrenome));
	}

}
