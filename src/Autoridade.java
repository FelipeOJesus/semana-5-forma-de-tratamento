
public class Autoridade {

	String nome;
	String sobrenome;
	String titulo;
	FormatadorNome tratamento;

	public FormatadorNome getTratamento() {
		tratamento = new FormatadorNome() {

			@Override
			public String formatarNome(String nome, String sobrenome) {
				return null;
			}

			@Override
			public String informal(String nome) {
				return nome;
			}

			@Override
			public String respeitoso(String sobrenome, boolean masculino) {
				if (masculino)
					return "Sr. " + sobrenome;
				return "Sra. " + sobrenome;
			}

			@Override
			public String comTitulo(String titulo, String nome, String sobrenome) {
				return titulo + " " + nome + " " + sobrenome;
			}
		};
		return tratamento;
	}

}
